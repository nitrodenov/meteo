package com.meteo.list

interface ListRouter {

    fun goToDetails(city: String)

}