package com.meteo.list

import android.app.Activity
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.jakewharton.rxrelay2.PublishRelay
import com.meteo.R
import com.meteo.list.adapter.ListAdapter
import com.meteo.remote.entities.Weather
import com.meteo.util.HorizontalDividerItemDecoration
import com.meteo.util.ViewSwitcher
import io.reactivex.Observable

interface ListView {

    val retryButtonClicks: Observable<Unit>

    val onListClicks: Observable<String>

    fun showContent(result: List<Weather>?)

    fun showProgress()

    fun showError()

}

class ListViewImpl(activity: Activity) : ListView {

    private val recyclerView = activity.findViewById<RecyclerView>(R.id.recycler_view)
    private val adapter: ListAdapter
    private val onListClick = PublishRelay.create<String>()
    private val viewSwitcher = ViewSwitcher(
            containerView = activity.findViewById(R.id.container),
            contentViewId = R.id.recycler_view)

    init {
        adapter = ListAdapter(onListClick = onListClick)
        recyclerView.adapter = adapter
        val layoutManager = LinearLayoutManager(recyclerView.context, LinearLayoutManager.VERTICAL, false)
        recyclerView.layoutManager = layoutManager
        val padding = activity.resources.getDimensionPixelSize(R.dimen.horizontal_indent)
        val divider = HorizontalDividerItemDecoration(
                divider = ContextCompat.getDrawable(activity, R.drawable.divider)!!,
                leftIndent = padding,
                rightIndent = padding
        )
        recyclerView.addItemDecoration(divider)
    }

    override val retryButtonClicks = viewSwitcher.refreshes()

    override val onListClicks = onListClick

    override fun showContent(data: List<Weather>?) {
        adapter.setData(data ?: emptyList())
        viewSwitcher.showContent()
    }

    override fun showProgress() {
        viewSwitcher.showProgress()
    }

    override fun showError() {
        viewSwitcher.showError()
    }

}