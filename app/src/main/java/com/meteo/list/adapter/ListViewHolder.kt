package com.meteo.list.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.meteo.R

class ListViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val logo: ImageView = view.findViewById(R.id.logo)
    val name: TextView = view.findViewById(R.id.name)
    val temperature: TextView = view.findViewById(R.id.temperature)

}