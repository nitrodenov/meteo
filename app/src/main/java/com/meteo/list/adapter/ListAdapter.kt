package com.meteo.list.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.jakewharton.rxrelay2.PublishRelay
import com.meteo.R
import com.meteo.remote.entities.Weather

class ListAdapter(
        private var weathers: List<Weather> = ArrayList(),
        private val onListClick: PublishRelay<String>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        val viewHolder = ListViewHolder(view)
        view.setOnClickListener {
            val position = viewHolder.adapterPosition
            val name = if (position == 0) {
                "my"
            } else {
                weathers[viewHolder.adapterPosition - 1].location?.name
            }
            onListClick.accept(name)
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            OUR_POSITION_VIEW_TYPE -> {
                val viewHolder = holder as ListViewHolder
                viewHolder.name.text = viewHolder.itemView.context.getString(R.string.my_location)
            }
            OTHER_VIEW_TYPE -> {
                val viewHolder = holder as ListViewHolder
                Glide.with(viewHolder.itemView.context)
                        .load("http:" + weathers[position - 1].current?.condition?.icon)
                        .into(viewHolder.logo)
                viewHolder.name.text = weathers[position - 1].location?.name
                val temp = weathers[position - 1].current?.temp
                viewHolder.temperature.text = if (temp?.compareTo(0) == 1) {
                    "+ " + temp.toString()
                } else {
                    temp.toString()
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return weathers.size + 1
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> OUR_POSITION_VIEW_TYPE
            else -> OTHER_VIEW_TYPE
        }
    }

    fun setData(data: List<Weather>) {
        weathers = data
        notifyDataSetChanged()
    }
}

private const val OUR_POSITION_VIEW_TYPE = 0
private const val OTHER_VIEW_TYPE = 1