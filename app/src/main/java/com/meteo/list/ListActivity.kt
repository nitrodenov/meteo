package com.meteo.list

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.meteo.MeteoApplication
import com.meteo.R
import com.meteo.details.createDetailsActivityIntent
import com.meteo.list.di.ListModule
import javax.inject.Inject

class ListActivity : AppCompatActivity(), ListRouter {

    @Inject
    lateinit var presenter: ListPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpActivityComponent(savedInstanceState)

        setContentView(R.layout.activity_list)

        val detailsView = ListViewImpl(this)

        presenter.attachView(detailsView)
    }

    override fun onStart() {
        super.onStart()

        presenter.attachRouter(this)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(KEY_STATE, presenter.getState())
    }

    override fun onStop() {
        super.onStop()

        presenter.detachRouter()
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.detachView()
    }

    override fun goToDetails(city: String) {
        startActivity(createDetailsActivityIntent(this, city))
    }

    private fun setUpActivityComponent(savedInstanceState: Bundle?) {
        val state = savedInstanceState?.getParcelable<Bundle>(KEY_STATE)
        MeteoApplication
                .instance
                .applicationComponent
                .plus(ListModule(state))
                .inject(this)
    }
}

const val KEY_DATA = "key_data"
const val KEY_STATE = "key_state"