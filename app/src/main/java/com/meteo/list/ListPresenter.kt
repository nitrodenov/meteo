package com.meteo.list

import android.os.Bundle
import com.meteo.remote.entities.Weather
import com.meteo.util.SchedulersFactory
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Function4
import io.reactivex.rxkotlin.plusAssign
import java.util.concurrent.TimeUnit

interface ListPresenter {

    fun attachView(view: ListView)

    fun detachView()

    fun attachRouter(router: ListRouter)

    fun detachRouter()

    fun getState(): Bundle

}

class ListPresenterImpl(
        private val schedulersFactory: SchedulersFactory,
        private val interactor: ListInteractor,
        bundle: Bundle?
) : ListPresenter {

    private var data: ArrayList<Weather>? = bundle?.getParcelableArrayList(KEY_DATA)
    private var view: ListView? = null
    private var router: ListRouter? = null
    private var dataDisposable: Disposable? = null
    private val viewDisposables = CompositeDisposable()

    override fun attachView(view: ListView) {
        this.view = view
        initializeView()
        if (data == null) {
            loadData()
        } else {
            view.showContent(data)
        }
    }

    override fun detachView() {
        this.view = null
        viewDisposables.clear()
        cancelDataRequest()
    }

    override fun attachRouter(router: ListRouter) {
        this.router = router
    }

    override fun detachRouter() {
        this.router = null
    }

    override fun getState(): Bundle {
        val bundle = Bundle()
        bundle.putParcelableArrayList(KEY_DATA, data)
        return bundle

    }

    private fun initializeView() {
        val view = this.view ?: return
        initializeRetryButtonClicks(view)
        initializeOnListButtonClicks(view)
    }

    private fun initializeRetryButtonClicks(view: ListView) {
        viewDisposables += view.retryButtonClicks
                .throttleFirst(300, TimeUnit.MILLISECONDS)
                .observeOn(schedulersFactory.mainThread())
                .subscribe {
                    loadData()
                }
    }

    private fun initializeOnListButtonClicks(view: ListView) {
        viewDisposables += view.onListClicks
                .throttleFirst(300, TimeUnit.MILLISECONDS)
                .observeOn(schedulersFactory.mainThread())
                .subscribe {
                    router?.goToDetails(it)
                }
    }

    private fun loadData() {
        view?.showProgress()
        dataDisposable?.dispose()

        val londonObservable = interactor.getCurrentWeather("London")
                .subscribeOn(schedulersFactory.io())
                .observeOn(schedulersFactory.mainThread())
        val parisObservable = interactor.getCurrentWeather("Paris")
                .subscribeOn(schedulersFactory.io())
                .observeOn(schedulersFactory.mainThread())
        val tokioObservable = interactor.getCurrentWeather("Tokio")
                .subscribeOn(schedulersFactory.io())
                .observeOn(schedulersFactory.mainThread())
        val newYorkObservable = interactor.getCurrentWeather("New-York")
                .subscribeOn(schedulersFactory.io())
                .observeOn(schedulersFactory.mainThread())
        Observable.zip(londonObservable, parisObservable, tokioObservable, newYorkObservable, Function4<Weather, Weather, Weather, Weather, List<Weather>> { t1, t2, t3, t4 ->
            listOf(t1, t2, t3, t4)
        }).subscribe(
                {
                    this.data = ArrayList(it)
                    view?.showContent(it)
                },
                {
                    view?.showError()
                }
        )
    }

    private fun cancelDataRequest() {
        dataDisposable?.dispose()
        dataDisposable = null
    }

}