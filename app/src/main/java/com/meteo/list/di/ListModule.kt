package com.meteo.list.di

import android.os.Bundle
import com.meteo.di.scope.PerActivity
import com.meteo.list.ListInteractor
import com.meteo.list.ListInteractorImpl
import com.meteo.list.ListPresenter
import com.meteo.list.ListPresenterImpl
import com.meteo.remote.MeteoApi
import com.meteo.util.SchedulersFactory
import dagger.Module
import dagger.Provides

@Module
class ListModule(private val state: Bundle?) {

    @Provides
    @PerActivity
    fun providePresenter(schedulersFactory: SchedulersFactory,
                         interactor: ListInteractor): ListPresenter {
        return ListPresenterImpl(schedulersFactory, interactor, state)
    }

    @Provides
    @PerActivity
    fun provideInteractor(api: MeteoApi): ListInteractor {
        return ListInteractorImpl(api)
    }
}