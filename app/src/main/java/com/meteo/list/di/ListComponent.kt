package com.meteo.list.di

import com.meteo.di.scope.PerActivity
import com.meteo.list.ListActivity
import dagger.Subcomponent

@PerActivity
@Subcomponent(modules = [(ListModule::class)])
interface ListComponent {

    fun inject(activity: ListActivity)

}