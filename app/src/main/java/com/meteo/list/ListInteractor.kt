package com.meteo.list

import com.meteo.remote.MeteoApi
import com.meteo.remote.entities.Weather
import io.reactivex.Observable

interface ListInteractor {

    fun getCurrentWeather(city: String): Observable<Weather>

}

class ListInteractorImpl(private val api: MeteoApi) : ListInteractor {

    override fun getCurrentWeather(city: String): Observable<Weather> {
        return api.getMeteo(city)
    }

}