package com.meteo.remote.entities

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Weather(
        @SerializedName("location") val location: Location?,
        @SerializedName("current") val current: Current?,
        @SerializedName("forecast") val forecast: Forecast?
) : Parcelable