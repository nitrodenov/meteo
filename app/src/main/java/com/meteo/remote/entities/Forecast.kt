package com.meteo.remote.entities

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Forecast(
        @SerializedName("forecastday") val forcasts: List<ForecastDay>?
) : Parcelable