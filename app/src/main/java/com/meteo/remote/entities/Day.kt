package com.meteo.remote.entities

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Day(
        @SerializedName("avgtemp_c") val temperature: Double?,
        @SerializedName("condition") val condition: Condition?
) : Parcelable