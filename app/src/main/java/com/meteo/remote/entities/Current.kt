package com.meteo.remote.entities

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Current(
        @SerializedName("last_updated") val lastUpdated: String,
        @SerializedName("temp_c") val temp: Double,
        @SerializedName("condition") val condition: Condition
) : Parcelable