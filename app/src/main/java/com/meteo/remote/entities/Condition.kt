package com.meteo.remote.entities

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Condition(
        @SerializedName("icon") val icon: String?
) : Parcelable