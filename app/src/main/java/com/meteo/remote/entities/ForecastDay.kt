package com.meteo.remote.entities

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class ForecastDay(
        @SerializedName("date") val date: String?,
        @SerializedName("day") val day: Day?
) : Parcelable