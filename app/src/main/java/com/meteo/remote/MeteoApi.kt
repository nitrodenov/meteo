package com.meteo.remote

import com.meteo.remote.entities.Weather
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface MeteoApi {

    @GET("v1/current.json?key=$METEO_API_KEY")
    fun getMeteo(
            @Query("q") city: String
    ): Observable<Weather>

    @GET("v1/forecast.json?key=$METEO_API_KEY")
    fun getMeteos(
            @Query("q") city: String,
            @Query("days") days: Int
    ): Observable<Weather>

}

const val BASE_URL = "http://api.apixu.com/"
const val METEO_API_KEY = "cf5e8e005f41490fa74103536181606"