package com.meteo

import android.app.Application
import com.meteo.di.component.ApplicationComponent
import com.meteo.di.component.DaggerApplicationComponent
import com.meteo.di.module.ApplicationModule

class MeteoApplication : Application() {

    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        instance = this

        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(ApplicationModule(this))
                .build()
        applicationComponent.inject(this)
    }

    companion object {
        @JvmStatic
        lateinit var instance: MeteoApplication
    }
}
