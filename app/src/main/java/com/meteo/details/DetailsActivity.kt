package com.meteo.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.meteo.MeteoApplication
import com.meteo.R
import com.meteo.details.di.DetailsModule
import javax.inject.Inject

class DetailsActivity : AppCompatActivity(), DetailsRouter {

    @Inject
    lateinit var presenter: DetailsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpActivityComponent(savedInstanceState)

        setContentView(R.layout.activity_details)

        val detailsView = DetailsViewImpl(this)

        presenter.attachView(detailsView)
    }

    override fun onStart() {
        super.onStart()

        presenter.attachRouter(this)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(KEY_STATE, presenter.getState())
    }

    override fun onStop() {
        super.onStop()

        presenter.detachRouter()
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.detachView()
    }

    override fun goBack() {
        finish()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        presenter.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun setUpActivityComponent(savedInstanceState: Bundle?) {
        val state = savedInstanceState?.getParcelable<Bundle>(KEY_STATE)
        val city = intent?.getStringExtra(KEY_CITY)
        MeteoApplication
                .instance
                .applicationComponent
                .plus(DetailsModule(state, city))
                .inject(this)
    }
}

const val KEY_DATA = "key_data"
const val KEY_STATE = "key_state"
const val KEY_CITY = "key_city"

fun createDetailsActivityIntent(context: Context, city: String): Intent {
    return Intent(context, DetailsActivity::class.java)
            .putExtra(KEY_CITY, city)

}