package com.meteo.details.di

import android.os.Bundle
import com.meteo.details.DetailsInteractor
import com.meteo.details.DetailsInteractorImpl
import com.meteo.details.DetailsPresenter
import com.meteo.details.DetailsPresenterImpl
import com.meteo.di.scope.PerActivity
import com.meteo.remote.MeteoApi
import com.meteo.util.SchedulersFactory
import dagger.Module
import dagger.Provides

@Module
class DetailsModule(private val state: Bundle?, private val city: String?) {

    @Provides
    @PerActivity
    fun providePresenter(schedulersFactory: SchedulersFactory,
                         interactor: DetailsInteractor): DetailsPresenter {
        return DetailsPresenterImpl(schedulersFactory, interactor, city, state)
    }

    @Provides
    @PerActivity
    fun provideInteractor(api: MeteoApi): DetailsInteractor {
        return DetailsInteractorImpl(api)
    }

}