package com.meteo.details.di

import com.meteo.details.DetailsActivity
import com.meteo.di.scope.PerActivity
import dagger.Subcomponent

@PerActivity
@Subcomponent(modules = [(DetailsModule::class)])
interface DetailsComponent {

    fun inject(activity: DetailsActivity)

}