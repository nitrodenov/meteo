package com.meteo.details

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.LocationManager
import android.support.v4.app.ActivityCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.widget.TextView
import com.jakewharton.rxbinding2.support.v7.widget.navigationClicks
import com.meteo.R
import com.meteo.details.adapter.DetailsAdapter
import com.meteo.remote.entities.Weather
import com.meteo.util.ViewSwitcher
import com.meteo.util.getLastKnownLocationCustom
import io.reactivex.Observable

interface DetailsView {

    val retryButtonClicks: Observable<Unit>

    fun showContent(data: Weather?)

    fun showProgress()

    fun showError()

    fun getMyLocation(): Pair<Double, Double>?

    fun getToolbarNavigationClicks(): Observable<Unit>

    fun setTitle(city: String?)

}

class DetailsViewImpl(private val activity: Activity) : DetailsView {

    private val currentTemperature = activity.findViewById<TextView>(R.id.current_temperature)
    private val recyclerView = activity.findViewById<RecyclerView>(R.id.recycler_view)
    private val toolbar = activity.findViewById<Toolbar>(R.id.toolbar)
    private val adapter: DetailsAdapter = DetailsAdapter()
    private val viewSwitcher = ViewSwitcher(
            containerView = activity.findViewById(R.id.container),
            contentViewId = R.id.content)

    init {
        toolbar.setNavigationIcon(R.drawable.ic_back)
        recyclerView.adapter = adapter
        val layoutManager = LinearLayoutManager(recyclerView.context, LinearLayoutManager.HORIZONTAL, false)
        recyclerView.layoutManager = layoutManager
    }

    override val retryButtonClicks = viewSwitcher.refreshes()

    override fun showContent(data: Weather?) {
        val temperature = data?.current?.temp
        this.currentTemperature.text = if (temperature?.compareTo(0) == 1) {
            "+ " + temperature.toString()
        } else {
            temperature.toString()
        }
        adapter.setData(data?.forecast?.forcasts)
        viewSwitcher.showContent()
    }

    override fun showProgress() {
        viewSwitcher.showProgress()
    }

    override fun showError() {
        viewSwitcher.showError()
    }

    override fun getMyLocation(): Pair<Double, Double>? {
        return if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            val locationManager = activity.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val myLocation = locationManager.getLastKnownLocationCustom()
            val latitude = myLocation?.latitude ?: 0.0
            val longitude = myLocation?.longitude ?: 0.0
            Pair(latitude, longitude)
        } else {
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSIONS_REQUEST_CODE)
            null
        }
    }

    override fun getToolbarNavigationClicks() = toolbar.navigationClicks()

    override fun setTitle(city: String?) {
        toolbar.title = if (city == "my") {
            activity.getString(R.string.my_location)
        } else {
            city
        }
    }
}

const val LOCATION_PERMISSIONS_REQUEST_CODE = 0