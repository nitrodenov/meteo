package com.meteo.details

import android.content.pm.PackageManager
import android.os.Bundle
import com.meteo.remote.entities.Weather
import com.meteo.util.SchedulersFactory
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import java.util.concurrent.TimeUnit

interface DetailsPresenter {

    fun attachView(view: DetailsView)

    fun detachView()

    fun attachRouter(router: DetailsRouter)

    fun detachRouter()

    fun getState(): Bundle

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray)

}

class DetailsPresenterImpl(
        private val schedulersFactory: SchedulersFactory,
        private val interactor: DetailsInteractor,
        private val city: String?,
        bundle: Bundle?
) : DetailsPresenter {

    private var data: Weather? = bundle?.getParcelable(KEY_DATA)
    private var view: DetailsView? = null
    private var router: DetailsRouter? = null
    private var dataDisposable: Disposable? = null
    private val viewDisposables = CompositeDisposable()

    override fun attachView(view: DetailsView) {
        this.view = view
        initializeView()
        if (data == null) {
            if (isMyLocation()) {
                val location = view.getMyLocation()
                if (location != null) {
                    loadData(location.first.toString() + "," + location.second.toString())
                }
            } else {
                loadData(city!!)
            }
        } else {
            view.showContent(data)
        }
    }

    override fun detachView() {
        this.view = null
        viewDisposables.clear()
        cancelDataRequest()
    }

    override fun attachRouter(router: DetailsRouter) {
        this.router = router
    }

    override fun detachRouter() {
        this.router = null
    }

    override fun getState(): Bundle {
        val bundle = Bundle()
        bundle.putParcelable(KEY_DATA, data)
        return bundle

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            LOCATION_PERMISSIONS_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    val location = view?.getMyLocation()
                    if (location != null) {
                        loadData(location.first.toString() + "," + location.second.toString())
                    }
                }
            }
        }
    }

    private fun initializeView() {
        val view = this.view ?: return
        view.setTitle(city)
        initializeRetryButtonClicks(view)
        initializeBackClicks(view)
    }

    private fun initializeRetryButtonClicks(view: DetailsView) {
        viewDisposables += view.retryButtonClicks
                .throttleFirst(300, TimeUnit.MILLISECONDS)
                .observeOn(schedulersFactory.mainThread())
                .subscribe {
                    loadData(city ?: "")
                }
    }

    private fun initializeBackClicks(view: DetailsView) {
        viewDisposables += view.getToolbarNavigationClicks()
                .throttleFirst(300, TimeUnit.MILLISECONDS)
                .observeOn(schedulersFactory.mainThread())
                .subscribe {
                    router?.goBack()

                }
    }

    private fun loadData(location: String) {
        view?.showProgress()
        dataDisposable?.dispose()

        dataDisposable = interactor.getWeather(location, 7)
                .subscribeOn(schedulersFactory.io())
                .observeOn(schedulersFactory.mainThread())
                .subscribe(
                        {
                            this.data = it
                            view?.showContent(data)
                        },
                        {
                            view?.showError()
                        }
                )

    }

    private fun isMyLocation(): Boolean {
        return city == "my"
    }

    private fun cancelDataRequest() {
        dataDisposable?.dispose()
        dataDisposable = null
    }

}
