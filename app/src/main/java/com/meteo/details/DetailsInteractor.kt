package com.meteo.details

import com.meteo.remote.MeteoApi
import com.meteo.remote.entities.Weather
import io.reactivex.Observable

interface DetailsInteractor {

    fun getWeather(city: String, days: Int): Observable<Weather>

}

class DetailsInteractorImpl(private val api: MeteoApi) : DetailsInteractor {

    override fun getWeather(city: String, days: Int): Observable<Weather> {
        return api.getMeteos(city, days)
    }

}