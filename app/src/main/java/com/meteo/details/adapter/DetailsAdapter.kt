package com.meteo.details.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.meteo.R
import com.meteo.remote.entities.ForecastDay
import java.text.SimpleDateFormat
import java.util.*

class DetailsAdapter(private var weathers: List<ForecastDay> = ArrayList()) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.details_item, parent, false)
        return DetailsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return weathers.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as DetailsViewHolder
        Glide.with(viewHolder.itemView.context)
                .load("http:" + weathers[position].day?.condition?.icon)
                .into(viewHolder.logo)
        val format = SimpleDateFormat("yyyy-MM-dd")
        val date = format.parse(weathers[position].date)
        val formatedDate = SimpleDateFormat("MMM d").format(date)
        viewHolder.date.text = formatedDate
        val temp = weathers[position].day?.temperature
        viewHolder.temperature.text = if (temp?.compareTo(0) == 1) {
            "+ " + temp.toString()
        } else {
            temp.toString()
        }
    }

    fun setData(weathers: List<ForecastDay>?) {
        this.weathers = weathers ?: emptyList()
        notifyDataSetChanged()
    }
}