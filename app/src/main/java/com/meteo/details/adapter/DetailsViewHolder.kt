package com.meteo.details.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.meteo.R

class DetailsViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val date: TextView = view.findViewById(R.id.date)
    val logo: ImageView = view.findViewById(R.id.logo)
    val temperature: TextView = view.findViewById(R.id.temperature)

}