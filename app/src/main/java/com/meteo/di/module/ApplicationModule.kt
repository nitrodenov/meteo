package com.meteo.di.module

import android.app.Application
import com.meteo.MeteoApplication
import com.meteo.util.SchedulersFactory
import com.meteo.util.SchedulersFactoryImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: MeteoApplication) {

    @Provides
    @Singleton
    fun provideApplication(): Application = application

    @Provides
    @Singleton
    fun provideSchedulersFactory(): SchedulersFactory = SchedulersFactoryImpl()

}