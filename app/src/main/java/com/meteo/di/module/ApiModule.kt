package com.meteo.di.module

import com.google.gson.Gson
import com.meteo.remote.BASE_URL
import com.meteo.remote.MeteoApi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(includes = [(ApplicationModule::class)])
class ApiModule {

    @Provides
    @Singleton
    fun provideCoffeeTopApi(gson: Gson,
                            httpClient: OkHttpClient): MeteoApi {
        val builder = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(httpClient)

        return builder
                .build()
                .create(MeteoApi::class.java)
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        val clientBuilder = OkHttpClient()
                .newBuilder()

        return clientBuilder.build()
    }

}