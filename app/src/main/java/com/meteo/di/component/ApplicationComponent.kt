package com.meteo.di.component

import com.meteo.MeteoApplication
import com.meteo.details.di.DetailsComponent
import com.meteo.details.di.DetailsModule
import com.meteo.di.module.ApiModule
import com.meteo.di.module.ApplicationModule
import com.meteo.di.module.GsonModule
import com.meteo.list.di.ListComponent
import com.meteo.list.di.ListModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(ApplicationModule::class), (ApiModule::class), (GsonModule::class)])
interface ApplicationComponent {

    fun inject(application: MeteoApplication)

    fun plus(listModule: ListModule): ListComponent

    fun plus(detailsModule: DetailsModule): DetailsComponent

}