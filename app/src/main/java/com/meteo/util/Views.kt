package com.meteo.util

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.view.View

fun View.crossfade(fadeOutView: View, duration: Long) {
    this.alpha = 0f
    this.visibility = View.VISIBLE

    this.animate()
            .alpha(1f)
            .setDuration(duration)
            .setListener(null)

    fadeOutView.animate()
            .alpha(0f)
            .setDuration(duration)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    fadeOutView.visibility = View.GONE
                }
            })

}
