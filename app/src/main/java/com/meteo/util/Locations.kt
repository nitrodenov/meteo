package com.meteo.util

import android.annotation.SuppressLint
import android.location.Location
import android.location.LocationManager

@SuppressLint("MissingPermission")
fun LocationManager.getLastKnownLocationCustom(): Location? {
    val providers = this.getProviders(true)
    var bestLocation: Location? = null
    for (provider in providers) {
        val l = this.getLastKnownLocation(provider) ?: continue
        if (bestLocation == null || l.accuracy < bestLocation.accuracy) {
            bestLocation = l
        }
    }
    return bestLocation
}