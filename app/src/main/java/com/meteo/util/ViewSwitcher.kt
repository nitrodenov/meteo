package com.meteo.util

import android.support.annotation.ColorInt
import android.support.annotation.IdRes
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.meteo.R
import io.reactivex.Observable

class ViewSwitcher @JvmOverloads constructor(
        private val containerView: ViewGroup,
        @IdRes var contentViewId: Int,
        @ColorInt private val backgroundColor: Int = ContextCompat.getColor(containerView.context, R.color.white)
) {

    private val inflater = LayoutInflater.from(containerView.context)
    private var progressView: View? = null
    private var errorView: View? = null
    private var listener: (() -> Unit)? = null

    fun showContent() {
        val contentView: View? = containerView.findViewById(contentViewId)
        if (this.progressView == null) {
            this.progressView = getProgressScreen()
        }
        val progressView = this.progressView
        containerView.removeView(progressView)

        if (this.errorView == null) {
            this.errorView = getErrorScreen()
        }
        val errorView = this.errorView
        containerView.removeView(errorView)

        if (progressView != null) {
            contentView?.crossfade(progressView, MATERIAL_ANIMATION_DURATION)
        }
        if (errorView != null) {
            contentView?.crossfade(errorView, MATERIAL_ANIMATION_DURATION)
        }
    }

    fun showProgress() {
        val contentView: View? = containerView.findViewById(contentViewId)
        if (this.progressView == null) {
            this.progressView = getProgressScreen()
        }
        val progressView = this.progressView
        containerView.removeView(progressView)
        containerView.addView(progressView)

        if (contentView != null) {
            progressView?.crossfade(contentView, 0)
        }
    }

    fun showError(message: String? = null) {
        val contentView: View? = containerView.findViewById(contentViewId)
        if (this.errorView == null) {
            this.errorView = getErrorScreen(message)
        }
        val errorView = this.errorView
        containerView.removeView(errorView)
        containerView.addView(errorView)

        if (contentView != null) {
            errorView?.crossfade(contentView, 0)
        }
    }

    fun refreshes(): Observable<Unit> {
        return Observable.create {
            setOnRefreshListener {
                it.onNext(Unit)
            }
            it.setCancellable { setOnRefreshListener(null) }
        }
    }

    fun setOnRefreshListener(listener: (() -> Unit)?) {
        this.listener = listener
    }

    private fun getProgressScreen(): View {
        val progress = inflater.inflate(R.layout.progress, containerView, false) as View
        progress.setBackgroundColor(backgroundColor)

        return progress
    }

    private fun getErrorScreen(message: String? = null): View {
        val error = inflater.inflate(R.layout.error, containerView, false) as View
        error.setBackgroundColor(backgroundColor)
        if (!message.isNullOrBlank()) {
            val errorText: TextView = error.findViewById(R.id.error_text)
            errorText.text = message
        }
        if (listener != null) {
            val errorButton: Button = error.findViewById(R.id.retry_button)
            errorButton.setOnClickListener { listener?.invoke() }
        }

        return error
    }
}

const val MATERIAL_ANIMATION_DURATION = 300L